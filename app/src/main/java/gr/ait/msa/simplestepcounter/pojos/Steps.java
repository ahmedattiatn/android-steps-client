package gr.ait.msa.simplestepcounter.pojos;

import java.util.Date;

/**
 * Created by apne on 20-07-17.
 */

public class Steps {
    private Long id;
    private Long version;
    private long steps;
    private Date date;
    private String user;

    public Steps(long steps, Date date, String user) {
        this.steps = steps;
        this.date = date;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public long getSteps() {
        return steps;
    }

    public void setSteps(long steps) {
        this.steps = steps;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Steps{" +
                "steps=" + steps +
                ", date=" + date +
                ", user='" + user + '\'' +
                '}';
    }
}
